desc 'i used this once to fix links after migrating to production environment from test'

namespace :project do

  include Rails.application.routes.url_helpers

  task modify_test_links_to_production: :environment do
	default_url_options[:protocol] = "https"
	default_url_options[:host] = "libertarian-party.ru"
	posts = Post.all
	posts.each do |post|
		if post.illustrations.nil?
			next
		end
		post.illustrations.each do |illustration|
			filename = illustration.picture.file.blob.filename
			find_to = 'https:\/\/libertarian-party\.ru\/rails\/active_storage\/blobs\/\w*--\w*\/' + filename.to_s
			post.body = post.body.sub(/#{find_to}/, Rails.application.routes.url_helpers.url_for(illustration.picture.file))
			post.save!
		end
	end	
  end

end
