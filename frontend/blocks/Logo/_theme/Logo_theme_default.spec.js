import React from 'react'
import { shallow } from 'enzyme'

import { Logo as BaseLogo } from '../Logo'
import { LogoThemeDefault } from './Logo_theme_default'

const Logo = LogoThemeDefault(BaseLogo)

describe('Logo_theme_default', () => {
  const logoThemeDefault = shallow(<Logo theme='default' />)

  it('has classes Logo and Logo_theme_default', () => {
    expect(logoThemeDefault.prop('className'))
      .toBe('Logo Logo_theme_default')
  })

  it('has default iconTheme', () => {
    expect(logoThemeDefault.prop('iconTheme')).toBe('default')
  })
})
