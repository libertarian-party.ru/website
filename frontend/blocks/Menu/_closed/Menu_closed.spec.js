import React from 'react'
import { shallow } from 'enzyme'

import { Menu as BaseMenu } from '../Menu'
import { MenuClosed } from './Menu_closed'

const Menu = MenuClosed(BaseMenu)

describe('Menu_closed', () => {
  const menuClosed = shallow(<Menu closed />)

  it('has classes Menu and Menu_closed', () => {
    expect(menuClosed.prop('className'))
      .toBe('Menu Menu_closed')
  })
})
