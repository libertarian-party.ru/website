# frozen_string_literal: true

class MembershipRequestsController < ApplicationController
  def new
    @membership_request = MembershipRequest.new
  end

  def create
    @membership_request = MembershipRequest.new(membership_request_params)

    if @membership_request.save
      @membership_request.email.present? &&
        MembershipRequestMailer.follow_up(@membership_request).deliver_now
      CreateCardOnTrelloJob.perform_later(@membership_request)
      NotifyWithTelegramJob.perform_later(@membership_request)
      redirect_to success_url
    else
      render :new
    end
  end

  private

  def membership_request_params
    params
      .require(:membership_request)
      .permit(
        :full_name,
        :email,
        :birthdate,
        :phone,
        :federal_subject_id,
        :residence,
        :district,
        :telegram,
        :occupation,
        :public_organizations,
        :comment
      )
  end
end
