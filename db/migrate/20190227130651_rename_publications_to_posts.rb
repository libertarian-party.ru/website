# frozen_string_literal: true

class RenamePublicationsToPosts < ActiveRecord::Migration[5.2]
  def change
    rename_table :publications, :posts
  end
end
