import React from 'react'
import { shallow } from 'enzyme'

import { FooterChatLink } from '../-ChatLink/Footer-ChatLink'
import { FooterMain } from './Footer-Main'
import { FooterParagraph } from '../-Paragraph/Footer-Paragraph'
import { FooterHeading } from '../-Heading/Footer-Heading'

describe('Footer-Main', () => {
  const footerMain = shallow(<FooterMain />)

  it('is <div>', () => {
    expect(footerMain.is('div')).toBe(true)
  })

  it('has class Footer-Main', () => {
    expect(footerMain.prop('className')).toBe('Footer-Main')
  })

  it('has Footer-Heading with text "Либертарианский публичный чат"', () => {
    const footerHeading = (
      <FooterHeading>Либертарианский публичный чат</FooterHeading>
    )
    expect(footerMain.contains(footerHeading)).toBe(true)
  })

  it('contains Footer-ChatLink and Footer-ChatLink_desktop', () => {
    const chatLinks = footerMain.find(FooterChatLink)
    expect(chatLinks.length).toBe(2)
    expect(chatLinks.find({ desktop: true }).length).toBe(1)
  })

  it('has Footer-Paragraph starting with "Чат, в котором"', () => {
    expect(footerMain.find(FooterParagraph).render().text())
      .toMatch(/^Чат, в\u00a0котором/)
  })
})
