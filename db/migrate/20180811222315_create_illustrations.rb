# frozen_string_literal: true

class CreateIllustrations < ActiveRecord::Migration[5.2]
  def change
    create_table :illustrations, id: :uuid do |t|
      t.string :caption
      t.references :publications, type: :uuid, index: {unique: true}
      t.timestamps null: true
    end
  end
end
