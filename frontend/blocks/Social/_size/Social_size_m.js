import { withBemMod } from '@bem-react/core'

import { cnSocial } from '../Social'
import './Social_size_m.css'

export const SocialSizeM = withBemMod(cnSocial(), { size: 'm' })
