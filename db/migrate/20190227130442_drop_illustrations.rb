# frozen_string_literal: true

class DropIllustrations < ActiveRecord::Migration[5.2]
  def change
    drop_table :illustrations
  end
end
