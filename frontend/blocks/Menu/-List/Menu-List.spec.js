import React from 'react'
import { shallow } from 'enzyme'

import { MenuItem } from '../-Item/Menu-Item'
import { MenuList } from './Menu-List'

describe('Menu-List', () => {
  const menuList = shallow(<MenuList />)

  it('is <ul>', () => {
    expect(menuList.is('ul')).toBe(true)
  })

  it('has class Menu-List', () => {
    expect(menuList.prop('className')).toBe('Menu-List')
  })

  it('has Menu-Item with uri /posts and text "Публикации"', () => {
    const item = (
      <MenuItem active={false} uri='/posts'>
        Публикации
      </MenuItem>
    )
    expect(menuList.contains(item)).toBe(true)
  })

  it('contains Menu-Item with uri /party and text "Партия"', () => {
    const item = <MenuItem active={false} uri='/party'>Партия</MenuItem>
    expect(menuList.contains(item)).toBe(true)
  })

  it('has Menu-Item "Либертарианство" with uri /libertarianism', () => {
    const item = (
      <MenuItem active={false} uri='/libertarianism'>
        Либертарианство
      </MenuItem>
    )
    expect(menuList.contains(item)).toBe(true)
  })

  it('includes Menu-Item with uri /donate and text "Поддержать"', () => {
    const item = <MenuItem active={false} uri='/donate'>Поддержать</MenuItem>
    expect(menuList.contains(item)).toBe(true)
  })

  it('contains Menu-Item "Вступить" with uri /join', () => {
    const item = <MenuItem active={false} uri='/join'>Вступить</MenuItem>
    expect(menuList.contains(item)).toBe(true)
  })
})

describe('Menu-List with activeItem set to "posts"', () => {
  const menuList = shallow(<MenuList activeItem='posts' />)

  it('has active Menu-Item with uri /posts', () => {
    const item = <MenuItem uri='/posts' active>Публикации</MenuItem>
    expect(menuList.contains(item)).toBe(true)
  })
})

describe('Menu-List with "party" as activeItem', () => {
  const menuList = shallow(<MenuList activeItem='party' />)

  it('includes Menu-Item with uri /party that is active', () => {
    const item = <MenuItem active uri='/party'>Партия</MenuItem>
    expect(menuList.contains(item)).toBe(true)
  })
})

describe('Menu-List with "libertarianism" as activeItem', () => {
  const menuList = shallow(<MenuList activeItem='libertarianism' />)

  it('has active Menu-Item with uri /libertarianism', () => {
    const item = (
      <MenuItem active uri='/libertarianism'>
        Либертарианство
      </MenuItem>
    )
    expect(menuList.contains(item)).toBe(true)
  })
})

describe('Menu-List with activeItem set to "donate"', () => {
  const menuList = shallow(<MenuList activeItem='donate' />)

  it('includes active Menu-Item with uri /donate', () => {
    const item = <MenuItem active uri='/donate'>Поддержать</MenuItem>
    expect(menuList.contains(item)).toBe(true)
  })
})

describe('Menu-List with activeItem set to "join"', () => {
  const menuList = shallow(<MenuList activeItem='join' />)

  it('has Menu-Item with uri /join that is active', () => {
    const item = <MenuItem active uri='/join'>Вступить</MenuItem>
    expect(menuList.contains(item)).toBe(true)
  })
})
