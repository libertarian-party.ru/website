module.exports = {
  moduleNameMapper: {
    '\\.(css|jpg|pdf|png|svg)$': '<rootDir>/mocks/static_assets.js'
  },
  setupTestFrameworkScriptFile: './enzyme.config.js',
  verbose: true
}
