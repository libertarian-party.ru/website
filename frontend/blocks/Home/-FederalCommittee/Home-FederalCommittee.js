import React from 'react'

import Contacts from '../../Contacts'
import Contact from '../../Contact'
import Link from '../../Link'

import { cnHome } from '../Home'

import akater from '../images/akater.jpg'
import akater2x from '../images/akater@2x.jpg'
import boiko from '../images/boiko.jpg'
import boiko2x from '../images/boiko@2x.jpg'
import kalyonov from '../images/kalyonov.jpg'
import kalyonov2x from '../images/kalyonov@2x.jpg'
import osenin from '../images/osenin.jpg'
import osenin2x from '../images/osenin@2x.jpg'
import ovsienko from '../images/ovsienko.jpg'
import ovsienko2x from '../images/ovsienko@2x.jpg'
import prohorov from '../images/prohorov.jpg'
import prohorov2x from '../images/prohorov@2x.jpg'
import samodurov from '../images/samodurov.jpg'
import samodurov2x from '../images/samodurov@2x.jpg'
import svetov from '../images/svetov.jpg'
import svetov2x from '../images/svetov@2x.jpg'
import conway from '../images/conway.jpg'
import conway2x from '../images/conway.jpg'

import romanov from '../images/romanov.jpg'
import romanov2x from '../images/romanov.jpg'
import grach from '../images/grach.jpg'
import grach2x from '../images/grach.jpg'
import maximov from '../images/maximov.jpg'
import maximov2x from '../images/maximov.jpg'
import chinarov from '../images/chinarov.jpg'
import chinarov2x from '../images/chinarov.jpg'
import rina from '../images/rina.jpg'
import rina2x from '../images/rina.jpg'                                                                  
import tolkien from '../images/tolkien.jpg'
import tolkien2x from '../images/tolkien.jpg'
import mariaivo from '../images/mariaivo.jpg'
import mariaivo2x from '../images/mariaivo.jpg'
import delugov from '../images/delugov.jpg'
import delugov2x from '../images/delugov.jpg'                                                            
import marat from '../images/marat.jpg'
import marat2x from '../images/marat.jpg'
import placeholder from '../images/placeholder.svg'
import placeholder2x from '../images/placeholder.svg'

import bazhenov from '../images/bazhenov.jpg'
import bazhenov2x from '../images/bazhenov@2x.jpg'
import fedyukin from '../images/fedyukin.jpg'
import fedyukin2x from '../images/fedyukin@2x.jpg'
import karnavsky from '../images/karnavsky.jpg'
import karnavsky2x from '../images/karnavsky@2x.jpg'
import khalilullin from '../images/khalilullin.jpg'
import khalilullin2x from '../images/khalilullin@2x.jpg'
import kotlyarova from '../images/kotlyarova.jpg'
import kotlyarova2x from '../images/kotlyarova@2x.jpg'
import nozdrin from '../images/nozdrin.jpg'
import nozdrin2x from '../images/nozdrin@2x.jpg'
import rudkovsky from '../images/rudkovsky.jpg'
import rudkovsky2x from '../images/rudkovsky@2x.jpg'
import shakhbazian from '../images/shakhbazian.jpg'
import shakhbazian2x from '../images/shakhbazian@2x.jpg'
import sharoglazov from '../images/sharoglazov.jpg'
import sharoglazov2x from '../images/sharoglazov@2x.jpg'
import yahontov from '../images/yahontov.jpg'
import yahontov2x from '../images/yahontov@2x.jpg'

export const HomeFederalCommittee = () => (
  <Contacts
    desc={
      <React.Fragment>
        основной руководящий орган, управляющий партией между съездами. {}
        <Link href='mailto:fk@libertarian-party.ru' theme='default'>
          fk@libertarian&#8209;party.ru
        </Link>
      </React.Fragment>
    }
    title='Федеральный комитет'
    withLabel>
    <Contact
      location='Самара'
      name='Борис Федюкин'
      photo={fedyukin}
      photo2x={fedyukin2x}
      position='Председатель'
      telegram='borisfedyukin' />
    <Contact
      location='Москва'
      name='Григорий Баженов'
      photo={bazhenov}
      photo2x={bazhenov2x}
      position='Заместитель председателя'
      telegram='bajenof' />
    <Contact
      location='Ямал'
      name='Сергей Карнавский'
      photo={karnavsky}
      photo2x={karnavsky2x}
      position='Федеральный секретарь'
      telegram='Siergiej' />
    <Contact
      location='Краснодар'
      name='Артур Шахбазян'
      photo={shakhbazian}
      photo2x={shakhbazian2x}
      position='Федеральный комитет'
      telegram='equals' />
    <Contact
      location='Рыбинск'
      name='Григорий Яхонтов'
      photo={yahontov}
      photo2x={yahontov2x}
      telegram='holyWatermaker'
      position='Федеральный комитет' />
    <Contact
      location='Новосибирск'
      name='Михаил Шароглазов'
      photo={sharoglazov}
      photo2x={sharoglazov2x}
      position='Федеральный комитет'
      telegram='m_shar' />
    <Contact
      telegram='wildstanvech89'
      location='Красноярск'
      name='Станислав Рудковский'
      photo={rudkovsky}
      photo2x={rudkovsky2x}
      position='Федеральный комитет' />
    <Contact
      telegram='irat_kh'
      location='Казань'
      name='Айрат Халилуллин'
      photo={khalilullin}
      photo2x={khalilullin2x}
      position='Федеральный комитет' />
    <Contact
      telegram='avgsta'
      location='Екатеринбург'
      name='Дарья Котлярова'
      photo={kotlyarova}
      photo2x={kotlyarova2x}
      position='Федеральный комитет' />
  </Contacts>
)
