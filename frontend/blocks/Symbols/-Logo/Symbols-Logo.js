import React from 'react'

import { cnSymbols } from '../Symbols'
import logo from '../images/logo.png'
import './Symbols-Logo.css'

export const SymbolsLogo = () => (
  <img className={cnSymbols('Logo')} src={logo} alt='' />
)
