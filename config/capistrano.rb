# frozen_string_literal: true

set :application, 'party_website'
set :repo_url, "git@gitlab.com:libertarian-party/#{fetch(:application)}.git"
set :branch, 'deploy'

set(:deploy_to, proc { "/var/www/#{fetch(:application)}/#{fetch(:stage)}" })
set(:user, proc { [fetch(:application), fetch(:stage)].join('_') })

set :linked_dirs, %w[log tmp/pids tmp/cache tmp/sockets public/assets storage]
set :keep_assets, 2
set :migration_role, :app

set :ssh_options, forward_agent: false
