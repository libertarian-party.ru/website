# frozen_string_literal: true

require 'test_helper'

class PostControllerTest < ActionDispatch::IntegrationTest
  def test_index_without_posts
    get posts_url
    assert_response :success
  end

  def test_index_with_posts
    create(:article, :published)
    create(:news, :published)
    get posts_url
    assert_response :success
    assert_select 'article.card', 2
  end

  def test_index_with_unpublished_posts
    create(:article)
    create(:news)
    get posts_url
    assert_response :success
    assert_select 'article.card', 0
  end

  def test_show_article
    article = create(:article, :published)
    get post_url(article)
    assert_response :success
  end

  def test_show_unpublished_article
    article = create(:article)
    assert_raise ActiveRecord::RecordNotFound do
      get post_url(article)
    end
  end

  def test_show_news
    news = create(:news, :published)
    get post_url(news)
    assert_response :success
  end

  def test_show_unpublished_news
    news = create(:news)
    assert_raise ActiveRecord::RecordNotFound do
      get post_url(news)
    end
  end

  def test_show_statement
    statement = create(:statement, :published)
    get post_url(statement)
    assert_response :success
  end

  def test_show_unpublished_statement
    statement = create(:statement)
    assert_raise ActiveRecord::RecordNotFound do
      get post_url(statement)
    end
  end
end
