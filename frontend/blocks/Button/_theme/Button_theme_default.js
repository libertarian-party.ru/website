import {withBemMod } from '@bem-react/core'

import { cnButton } from '../Button'
import './Button_theme_default.css'

export const ButtonThemeDefault = withBemMod(cnButton(), { theme: 'default' })
