# frozen_string_literal: true

require 'test_helper'

module Admin
  class PostsControllerTest < ActionDispatch::IntegrationTest
    def setup
      user = create(:user, password: 'password')
      login(email: user.email, password: 'password')
    end

    def attributes_for_thumbnail
      class_under_test = self.class.to_s[/(\w*)Test$/, 1]
      path = File.join('files', "#{class_under_test.downcase}.jpg")

      thumb_attrs = attributes_for(:picture)
      thumb_attrs[:file] = fixture_file_upload(path, 'image/jpeg')

      {thumbnail_attributes: thumb_attrs}
    end

    class GeneralTest < Admin::PostsControllerTest
      def test_index_with_no_posts
        get admin_posts_url
        assert_response :success
      end

      def test_index_with_some_posts
        create(:statement, :published)
        create(:news, :published)
        get admin_posts_url
        assert_response :success
      end

      def test_new
        get new_admin_post_url
        assert_response :success
      end
    end

    class ArticleTest < PostsControllerTest
      def test_create
        params = {
          article: attributes_for(:article).merge(attributes_for_thumbnail),
          type: 'Article'
        }

        assert_difference('Article.count') do
          post admin_posts_url, params: params
        end
        assert_redirected_to edit_admin_post_url(Article.last.id)
      end

      def test_edit
        article = create(:article)
        get edit_admin_post_url(article)
        assert_response :success
      end

      def test_update
        article = create(:article)
        params = {
          article: {
            title: 'Some other title'
          },
          type: 'Article'
        }

        patch admin_post_url(article), params: params
        assert_redirected_to admin_posts_url
      end

      def test_update_thumbnail
        article = create(:article)
        params = {
          article: attributes_for_thumbnail,
          type: 'Article'
        }

        patch admin_post_url(article), params: params
        assert_redirected_to admin_posts_url
      end

      def test_destroy
        article = create(:article)
        assert_difference('Article.count', -1) do
          delete admin_post_url(article)
        end
        assert_redirected_to admin_posts_url
      end
    end

    class NewsTest < PostsControllerTest
      def test_create
        params = {
          news: attributes_for(:news).merge(attributes_for_thumbnail),
          type: 'News'
        }

        assert_difference('News.count') do
          post admin_posts_url, params: params
        end
        assert_redirected_to edit_admin_post_url(News.last.id)
      end

      def test_edit
        news = create(:news)
        get edit_admin_post_url(news)
        assert_response :success
      end

      def test_update
        news = create(:news)
        params = {
          news: {
            title: 'Some other title'
          },
          type: 'News'
        }

        patch admin_post_url(news), params: params
        assert_redirected_to admin_posts_url
      end

      def test_update_thumbnail
        news = create(:news)
        params = {
          news: attributes_for_thumbnail,
          type: 'News'
        }

        patch admin_post_url(news), params: params
        assert_redirected_to admin_posts_url
      end

      def test_destroy
        news = create(:news)
        assert_difference('News.count', -1) do
          delete admin_post_url(news)
        end
        assert_redirected_to admin_posts_url
      end
    end

    class StatementTest < PostsControllerTest
      def test_create
        params = {
          statement: attributes_for(:statement).merge(attributes_for_thumbnail),
          type: 'Statement'
        }

        assert_difference('Statement.count') do
          post admin_posts_url, params: params
        end
        assert_redirected_to edit_admin_post_url(Statement.last.id)
      end

      def test_edit
        statement = create(:statement)
        get edit_admin_post_url(statement)
        assert_response :success
      end

      def test_update
        statement = create(:statement)
        params = {
          statement: {
            title: 'Some other title'
          },
          type: 'Statement'
        }

        patch admin_post_url(statement), params: params
        assert_redirected_to admin_posts_url
      end

      def test_update_thumbnail
        statement = create(:statement)
        params = {
          statement: attributes_for_thumbnail,
          type: 'Statement'
        }

        patch admin_post_url(statement), params: params
        assert_redirected_to admin_posts_url
      end

      def test_destroy
        statement = create(:statement)
        assert_difference('Statement.count', -1) do
          delete admin_post_url(statement)
        end
        assert_redirected_to admin_posts_url
      end
    end
  end
end
