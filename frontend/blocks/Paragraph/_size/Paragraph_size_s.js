import { withBemMod } from '@bem-react/core'

import { cnParagraph } from '../Paragraph'
import './Paragraph_size_s.css'

export const ParagraphSizeS = withBemMod(cnParagraph(), { size: 's' })
