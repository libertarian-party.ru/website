import React from 'react'
import { shallow } from 'enzyme'

import { Button as BaseButton } from '../Button'
import { ButtonIcon } from '../-Icon/Button-Icon'
import { ButtonText } from '../-Text/Button-Text'
import { ButtonTypeDisabled } from './Button_type_disabled'

const Button = ButtonTypeDisabled(BaseButton)

describe('Button_type_disabled', () => {
  const buttonTypeDisabled = shallow(<Button type='disabled' />)

  it('is <div>', () => {
    expect(buttonTypeDisabled.is('div')).toBe(true)
  })

  it('has classes Button and Button_type_disabled by default', () => {
    expect(buttonTypeDisabled.prop('className'))
      .toBe('Button Button_type_disabled')
  })

  it('by default has no Button-Icon', () => {
    expect(buttonTypeDisabled.exists('.Button-Icon')).toBe(false)
  })

  it('contains an empty Button-Text by default', () => {
    expect(buttonTypeDisabled.contains(<ButtonText />)).toBe(true)
  })
})

describe('Button_type_disabled with extra classes', () => {
  const buttonTypeDisabled = shallow(
    <Button type='disabled' className='Hey' />
  )

  it('appends them to <a>', () => {
    expect(buttonTypeDisabled.prop('className'))
      .toBe('Button Button_type_disabled Hey')
  })
})

describe('Button_type_disabled with an icon', () => {
  const buttonTypeDisabled = shallow(<Button type='disabled' icon={<svg />} />)

  it('passes that to Button-Icon', () => {
    expect(buttonTypeDisabled.contains(<ButtonIcon><svg /></ButtonIcon>))
      .toBe(true)
  })
})

describe('Button_type_disabled with text', () => {
  const buttonTypeDisabled = shallow(<Button type='disabled'>hi</Button>)

  it('passes them to the underlying Button-Text', () => {
    expect(buttonTypeDisabled.contains(<ButtonText>hi</ButtonText>)).toBe(true)
  })
})
