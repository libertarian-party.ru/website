# frozen_string_literal: true

module Admin
  class UsersController < BaseController
    skip_before_action :require_login, only: %i[new create]

    before_action :require_admin, only: %i[index destroy]
    before_action :set_user, only: %i[edit update destroy]
    before_action :set_invite, only: %i[new create]
    before_action :require_valid_invite, only: %i[new create]

    def index
      @users = User.reorder(created_at: :desc).page(params[:page]).per(9)
    end

    def new
      @user = User.new
    end

    def create
      @user = User.new(user_params)
      @user.invite = @invite

      if @user.save
        redirect_to login_url, notice: t('.success')
      else
        render :new
      end
    end

    def edit
      require_admin unless @user == current_user
    end

    def update
      require_admin && return unless @user == current_user

      if @user.update(user_params)
        redirect_to admin_users_url, notice: t('.success')
      else
        render :edit
      end
    end

    def destroy
      @user.destroy
      redirect_to admin_users_url, notice: t('.success')
    end

    private

    def set_user
      @user = User.find(params[:id])
    end

    def set_invite
      @invite = Invite.find_by(id: params[:invite])
    end

    def require_valid_invite
      render :problem_with_invite, status: :bad_request unless @invite&.fresh?
    end

    def user_params
      params
        .require(:user)
        .permit(
          :name,
          :email,
          :password,
          :password_confirmation
        )
    end
  end
end
