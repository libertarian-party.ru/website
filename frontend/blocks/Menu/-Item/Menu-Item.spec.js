import React from 'react'
import { shallow } from 'enzyme'

import Link from '../../Link'
import { BaseMenuItem as MenuItem } from './Menu-Item'

describe('Menu-Item', () => {
  const menuItem = shallow(<MenuItem />)

  it('is <li>', () => {
    expect(menuItem.is('li')).toBe(true)
  })

  it('has class Menu-Item by default', () => {
    expect(menuItem.prop('className')).toBe('Menu-Item')
  })

  it('contains a blank Link by default', () => {
    expect(menuItem.contains(<Link />)).toBe(true)
  })
})

describe('Menu-Item with additional class', () => {
  const menuItem = shallow(<MenuItem className='Menu-Item_mod' />)

  it('adds that class to the existing one', () => {
    expect(menuItem.prop('className')).toBe('Menu-Item Menu-Item_mod')
  })
})

describe('Menu-Item with children', () => {
  const menuItem = shallow(<MenuItem>About us</MenuItem>)

  it('passes them to Link', () => {
    expect(menuItem.contains(<Link>About us</Link>)).toBe(true)
  })
})

describe('Menu-Item with URI', () => {
  const menuItem = shallow(<MenuItem uri='/about' />)

  it('passes that URI as href to Link', () => {
    expect(menuItem.contains(<Link href='/about' />)).toBe(true)
  })
})
