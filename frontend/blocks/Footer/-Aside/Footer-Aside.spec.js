import React from 'react'
import { shallow } from 'enzyme'

import Button from '../../Button'
import { FooterAside } from './Footer-Aside'
import { FooterHeading } from '../-Heading/Footer-Heading'
import { FooterSocialMedia } from '../-SocialMedia/Footer-SocialMedia'

describe('Footer-Aside', () => {
  const footerAside = shallow(<FooterAside />)

  it('is <div>', () => {
    expect(footerAside.is('div')).toBe(true)
  })

  it('has class Footer-Aside', () => {
    expect(footerAside.prop('className')).toBe('Footer-Aside')
  })

  it('has Footer-Heading containing text "Каналы и другие медиа"', () => {
    const footerHeading = <FooterHeading>Каналы и другие медиа</FooterHeading>
    expect(footerAside.contains(footerHeading)).toBe(true)
  })

  it('contains Footer-SocialMedia', () => {
    const footerSocialMedia = <FooterSocialMedia />
    expect(footerAside.contains(footerSocialMedia)).toBe(true)
  })

  it('includes Button with uri /press and text "Для прессы"', () => {
    const pressButton = <Button type='link' uri='/press'>Для прессы</Button>
    expect(footerAside.containsMatchingElement(pressButton)).toBe(true)
  })
})

describe('Footer-Aside with isPressButtonDisabled set to true', () => {
  const footerAside = shallow(<FooterAside isPressButtonDisabled />)

  it('now includes disabled Button with text "Для прессы"', () => {
    const pressButton = <Button type='disabled'>Для прессы</Button>
    expect(footerAside.containsMatchingElement(pressButton)).toBe(true)
  })
})
