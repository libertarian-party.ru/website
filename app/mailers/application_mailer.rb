# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'notifications@new.libertarian-party.ru'
  layout 'mailer'
end
