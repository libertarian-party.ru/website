import React from 'react'
import { shallow } from 'enzyme'

import { ButtonText } from './Button-Text'

describe('Button-Text', () => {
  const buttonText = shallow(<ButtonText />)

  it('is <span>', () => {
    expect(buttonText.is('span')).toBe(true)
  })

  it('has class Button-Text', () => {
    expect(buttonText.prop('className')).toBe('Button-Text')
  })

  it('has no children by default', () => {
    expect(buttonText.children().length).toBe(0)
  })
})

describe('Button-Text with a child', () => {
  const buttonText = shallow(<ButtonText>child</ButtonText>)

  it('contains the child', () => {
    expect(buttonText.children().length).toBe(1)
    expect(buttonText.text()).toBe('child')
  })
})
