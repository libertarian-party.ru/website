import React from 'react'
import { shallow } from 'enzyme'

import { Logo as BaseLogo } from '../Logo'
import { LogoThemeNegative } from './Logo_theme_negative'

const Logo = LogoThemeNegative(BaseLogo)

describe('Logo_theme_negative', () => {
  const logoThemeNegative = shallow(<Logo theme='negative' />)

  it('has classes Logo and Logo_theme_negative', () => {
    expect(logoThemeNegative.prop('className'))
      .toBe('Logo Logo_theme_negative')
  })

  it('has negative iconTheme', () => {
    expect(logoThemeNegative.prop('iconTheme')).toBe('negative')
  })
})
