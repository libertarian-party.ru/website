import { withBemMod } from '@bem-react/core'

import { cnHeading } from '../Heading'
import './Heading_size_xl.css'

export const HeadingSizeXL = withBemMod(cnHeading(), { size: 'xl' })
