# frozen_string_literal: true

class RecreateIllustrations < ActiveRecord::Migration[5.2]
  def change
    create_table :illustrations, id: :uuid do |t|
      t.string :alt
      t.references :post, type: :uuid
      t.timestamps null: true
    end

    remove_column :pictures, :alt, :string
  end
end
