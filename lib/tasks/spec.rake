# frozen_string_literal: true

namespace :project do
  namespace :spec do
    desc 'Run JS specs only'
    task :js do
      sh 'bin/yarn jest --config frontend/jest.config.js'
    end
  end

  desc 'Run all specs'
  task spec: %i[spec:js]
end
