# frozen_string_literal: true

class CreateNews < ActiveRecord::Migration[5.1]
  def change
    create_table :news, id: :uuid do |t|
      t.string :title
      t.text :body
      t.datetime :published_at

      t.timestamps
    end
  end
end
