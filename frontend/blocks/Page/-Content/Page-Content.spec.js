import React from 'react'
import { shallow } from 'enzyme'

import { PageContent } from './Page-Content'

describe('Page-Content', () => {
  const pageContent = shallow(<PageContent />)

  it('is <div> by default', () => {
    expect(pageContent.is('div')).toBe(true)
  })

  it('has class Page-Content', () => {
    expect(pageContent.prop('className')).toBe('Page-Content')
  })

  it('has undefined tag by default', () => {
    expect(pageContent.prop('tag')).toBe(undefined)
  })

  it('is empty by default', () => {
    expect(pageContent.children().length).toBe(0)
  })
})

describe('Page-Content with different tag, for example main', () => {
  const pageContent = shallow(<PageContent tag='main' />)

  it('is now <main>', () => {
    expect(pageContent.is('main')).toBe(true)
  })
})

describe('Page-Content with children', () => {
  const pageContent = shallow(<PageContent><div /></PageContent>)

  it('is not empty anymore', () => {
    expect(pageContent.children().length).toBe(1)
    expect(pageContent.contains(<div />)).toBe(true)
  })
})
