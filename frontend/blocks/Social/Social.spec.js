import React from 'react'
import { shallow } from 'enzyme'

import { Social } from './Social'

describe('Social', () => {
  const social = shallow(<Social />)

  it('is <a>', () => {
    expect(social.is('a')).toBe(true)
  })

  it('has class Social by default', () => {
    expect(social.prop('className')).toBe('Social')
  })

  it('by default has no icon inside', () => {
    expect(social.prop('icon')).toBeUndefined()
    expect(social.children().length).toBe(0)
  })

  it('by default has empty href', () => {
    expect(social.prop('href')).toBeUndefined()
  })
})

describe('Social with additional classes', () => {
  const social = shallow(<Social className='Some Classes' />)

  it('appends them to the Social class', () => {
    expect(social.prop('className')).toBe('Social Some Classes')
  })
})

describe('Social with an icon', () => {
  const social = shallow(<Social icon={<svg />} />)

  it('includes the icon inside <a>', () => {
    expect(social.contains(<svg />)).toBe(true)
    expect(social.children().length).toBe(1)
  })
})

describe('Social with some URI', () => {
  const social = shallow(<Social uri='https://foo.example' />)

  it('passes that URI to href', () => {
    expect(social.prop('href')).toBe('https://foo.example')
  })
})
