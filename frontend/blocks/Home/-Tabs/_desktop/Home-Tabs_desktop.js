import { withBemMod } from '@bem-react/core'

import { cnHome } from '../../Home'
import './Home-Tabs_desktop.css'

export const HomeTabsDesktop = withBemMod(cnHome('Tabs'), { desktop: true })
