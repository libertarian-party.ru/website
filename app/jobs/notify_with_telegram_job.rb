# frozen_string_literal: true

class NotifyWithTelegramJob < ApplicationJob
  queue_as :telegram

  def perform(membership_request)
    membership_request = present(membership_request)
    message = membership_request.render

    notify(ENV['TELEGRAM_FEDERAL_CHAT'], message)

    chat = membership_request.federal_subject.regional_branch&.telegram_chat
    notify(chat, message) if chat
  end

  private

  def notify(chat, message)
    params = {
      chat_id: chat,
      text: message
    }

    Net::HTTP.post_form(
      URI("https://api.telegram.org/bot#{ENV['TELEGRAM_TOKEN']}/sendMessage"),
      params
    )
  end
end
