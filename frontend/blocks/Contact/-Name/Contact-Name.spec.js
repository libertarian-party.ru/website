import React from 'react'
import { shallow } from 'enzyme'

import { ContactName } from './Contact-Name'

describe('Contact-Name', () => {
  const contactName = shallow(<ContactName />)

  it('is <div>', () => {
    expect(contactName.is('div')).toBe(true)
  })

  it('has class Contact-Name', () => {
    expect(contactName.prop('className')).toBe('Contact-Name')
  })

  it('has got no child', () => {
    expect(contactName.children().length).toBe(0)
  })
})

describe('Contact-Name with children', () => {
  const contactName = shallow(<ContactName>Some Name</ContactName>)

  it('now has a child', () => {
    expect(contactName.children().length).toBe(1)
    expect(contactName.text()).toBe('Some Name')
  })
})
