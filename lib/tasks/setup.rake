# frozen_string_literal: true

file '.env' do
  cp 'config/examples/dotenv', '.env'
end

namespace :project do
  namespace :setup do
    task :deps do
      sh 'bin/bundle install --without production'
      sh 'bin/yarn install'
    end

    task :db do
      sh 'bin/rails db:setup'
      sh 'RAILS_ENV=test DATABASE_URL=postgresql://postgres:123Qwer@localhost/party_website_test bin/rails db:setup', unsetenv_others: false
    end
  end

  desc 'Make the project ready for hacking'
  task setup: %i[.env setup:deps setup:db]
end
