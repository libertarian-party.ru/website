import React from 'react'
import { shallow } from 'enzyme'

import { LogoText } from './Logo-Text'

describe('Logo-Text', () => {
  const logoText = shallow(<LogoText />)

  it('is <span>', () => {
    expect(logoText.is('span')).toBe(true)
  })

  it('has class Logo-Text', () => {
    expect(logoText.prop('className')).toBe('Logo-Text')
  })

  it('has no children by default', () => {
    expect(logoText.children().length).toBe(0)
  })
})

describe('Logo-Text with children', () => {
  const logoText = shallow(<LogoText>hello there</LogoText>)

  it('contains provided children', () => {
    expect(logoText.children().length).toBe(1)
    expect(logoText.text()).toBe('hello there')
  })
})
