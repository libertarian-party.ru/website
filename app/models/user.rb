# frozen_string_literal: true

class User < ApplicationRecord
  belongs_to :invite
  has_many :posts, inverse_of: 'author'

  authenticates_with_sorcery!

  before_validation :downcase_email
  before_validation :set_admin_to_false

  validates :email, presence: true, uniqueness: true
  validates :name, presence: true, uniqueness: {case_sensitive: false}
  validates :invite, presence: true, uniqueness: true
  with_options if: :need_to_check_password? do
    validates :password, length: {minimum: 8}, confirmation: true
    validates :password_confirmation, presence: true
  end

  private

  def downcase_email
    email&.downcase!
  end

  def set_admin_to_false
    self.admin = false
  end

  def need_to_check_password?
    new_record? || changes[:encrypted_password]
  end
end
