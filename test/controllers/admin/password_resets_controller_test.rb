# frozen_string_literal: true

require 'test_helper'

module Admin
  class PasswordResetsControllerTest < ActionDispatch::IntegrationTest
    def test_new
      get new_admin_password_reset_url
      assert_response :success
    end

    def test_create
      user = create(:user)
      params = {email: user.email}
      post admin_password_resets_url, params: params
      assert_redirected_to login_url
    end
  end
end
