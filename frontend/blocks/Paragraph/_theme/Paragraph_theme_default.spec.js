import React from 'react'
import { shallow } from 'enzyme'

import { Paragraph as BaseParagraph } from '../Paragraph'
import { ParagraphThemeDefault } from './Paragraph_theme_default'

const Paragraph = ParagraphThemeDefault(BaseParagraph)

describe('Paragraph_theme_default', () => {
  const paragraph = shallow(<Paragraph theme='default' />)

  it('has classes Paragraph and Paragraph_theme_default', () => {
    expect(paragraph.prop('className'))
      .toBe('Paragraph Paragraph_theme_default')
  })
})
