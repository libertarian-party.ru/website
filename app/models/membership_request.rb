# frozen_string_literal: true

class MembershipRequest < ApplicationRecord
  belongs_to :federal_subject

  validates :full_name, presence: true
  validates :birthdate, presence: true
  validates :phone, presence: true, format: {with: /\A\+\d{8,15}\z/}
  validates :federal_subject_id, presence: true
  validates :residence, presence: true
  validates :email, format: {with: /\A[^@]+@[^@]+\z/}, allow_blank: true

  validate :unique_for_last_fifteen_minutes
  validate :birthdate_not_in_future

  before_validation :strip_spaces
  before_validation :normalize_telegram

  private

  def unique_for_last_fifteen_minutes
    params = {
      full_name: full_name,
      phone: phone,
      email: email,
      telegram: telegram,
      since: 15.minutes.ago,
      to: Time.now
    }

    query = <<~SQL
      created_at BETWEEN :since AND :to
      AND (LOWER(full_name) = LOWER(:full_name)
           OR phone = :phone
           OR NOT COALESCE(email, '') = ''
              AND LOWER(email) = LOWER(:email)
           OR NOT COALESCE(telegram, '') = ''
              AND LOWER(telegram) = LOWER(:telegram))
    SQL

    return if MembershipRequest.where(query, params).blank?

    errors.add(:base, :duplicate)
  end

  def birthdate_not_in_future
    return if birthdate.blank?

    errors.add(:birthdate, :in_future) if birthdate >= Date.today
  end

  def fields_to_strip
    %i[
      comment
      district
      email
      full_name
      phone
      occupation
      public_organizations
      residence
      telegram
    ]
  end

  def strip_spaces
    fields_to_strip.each { |attr| self[attr]&.strip! }
  end

  def normalize_telegram
    telegram.sub!(%r{(https?://)?t.me/}, '') if telegram.present?
  end
end
