document.addEventListener("turbolinks:load", function() {
  var input = document.getElementById("thumb-upload");

  if (input !== null) {
    var preview = document.getElementById("thumb");

    input.addEventListener("change", function() {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.addEventListener("load", function(e) {
          preview.src = e.target.result;
        })

        reader.readAsDataURL(input.files[0]);
      }
    })
  }
})
