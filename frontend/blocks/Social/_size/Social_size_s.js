import { withBemMod } from '@bem-react/core'

import { cnSocial } from '../Social'
import './Social_size_s.css'

export const SocialSizeS = withBemMod(cnSocial(), { size: 's' })
