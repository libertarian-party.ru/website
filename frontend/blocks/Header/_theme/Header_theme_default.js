import React from 'react'
import { withBemMod } from '@bem-react/core'

import { cnHeader } from '../Header'
import './Header_theme_default.css'

const newBody = (Base, props) => {
  const newProps = Object.assign({}, props)
  newProps.burgerTheme = 'default'
  newProps.logoTheme = 'default'

  return <Base {...newProps} />
}

export const HeaderThemeDefault = withBemMod(
  cnHeader(),
  { theme: 'default' },
  newBody
)
