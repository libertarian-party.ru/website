# frozen_string_literal: true

require 'test_helper'

class MembershipRequestTest < ActiveSupport::TestCase
  def test_that_factory_defines_valid_membership_request
    membership_request = build(:membership_request)
    assert membership_request.valid?
  end

  def test_full_name_presence_validation
    membership_request = build(:membership_request, full_name: '')
    refute membership_request.valid?
  end

  def test_if_blank_birthdate_is_fine
    membership_request = build(:membership_request, birthdate: nil)
    refute membership_request.valid?
  end

  def test_when_birthdate_is_not_actually_date
    membership_request = build(:membership_request, birthdate: 'not date')
    refute membership_request.valid?
  end

  def test_that_birthdate_in_future_is_not_okay
    membership_request = build(:membership_request, birthdate: Date.tomorrow)
    refute membership_request.valid?
  end

  def test_membership_request_without_phone
    membership_request = build(:membership_request, phone: '')
    refute membership_request.valid?
  end

  def test_with_phone_in_wrong_format
    membership_request = build(:membership_request, phone: '8 (123) 456-78-90')
    refute membership_request.valid?
  end

  def test_that_federal_subject_is_required
    membership_request = build(:membership_request, federal_subject_id: nil)
    refute membership_request.valid?
  end

  def test_if_residence_presence_is_checked
    membership_request = build(:membership_request, residence: '')
    refute membership_request.valid?
  end

  def test_that_blank_email_is_allowed
    membership_request = build(:membership_request, email: '')
    assert membership_request.valid?
  end

  def test_membership_request_with_wrong_email_format
    membership_request = build(:membership_request, email: '@email.example')
    refute membership_request.valid?
  end

  def test_that_two_unique_membership_requests_can_be_sent
    create(
      :membership_request,
      full_name: 'Some name',
      email: 'some@email.example',
      phone: '+71234567890',
      telegram: '+71234567890'
    )
    membership_request = build(
      :membership_request,
      full_name: 'Other name',
      email: 'other@email.example',
      phone: '+70987654321',
      telegram: '+70987654321'
    )
    assert membership_request.valid?
  end

  def test_what_about_two_membership_requests_with_duplicate_full_name
    name = 'Some name'
    create(
      :membership_request,
      full_name: name,
      email: 'some@email.example',
      phone: '+71234567890',
      telegram: '+71234567890'
    )
    membership_request = build(
      :membership_request,
      full_name: name,
      email: 'other@email.example',
      phone: '+70987654321',
      telegram: '+70987654321'
    )
    refute membership_request.valid?
  end

  def test_same_full_name_fifteen_minutes_later
    name = 'Some name'
    create(
      :membership_request,
      full_name: name,
      email: 'some@email.example',
      phone: '+71234567890',
      telegram: '+71234567890',
      created_at: 15.minutes.ago
    )
    membership_request = build(
      :membership_request,
      full_name: name,
      email: 'other@email.example',
      phone: '+70987654321',
      telegram: '+70987654321'
    )
    assert membership_request.valid?
  end

  def test_case_sensivity_of_full_name_uniqueness_validation
    name = 'Some name'
    create(
      :membership_request,
      full_name: name.upcase,
      email: 'some@email.example',
      phone: '+71234567890',
      telegram: '+71234567890'
    )
    membership_request = build(
      :membership_request,
      full_name: name,
      email: 'other@email.example',
      phone: '+70987654321',
      telegram: '+70987654321'
    )
    refute membership_request.valid?
  end

  def test_two_membership_requests_with_same_email
    email = 'some@email.example'
    create(
      :membership_request,
      full_name: 'Some name',
      email: email,
      phone: '+71234567890',
      telegram: '+71234567890'
    )
    membership_request = build(
      :membership_request,
      full_name: 'Other name',
      email: email,
      phone: '+70987654321',
      telegram: '+70987654321'
    )
    refute membership_request.valid?
  end

  def test_existing_email_after_fifteen_minutes
    email = 'some@email.example'
    create(
      :membership_request,
      full_name: 'Some name',
      email: email,
      phone: '+71234567890',
      telegram: '+71234567890',
      created_at: 15.minutes.ago
    )
    membership_request = build(
      :membership_request,
      full_name: 'Other name',
      email: email,
      phone: '+70987654321',
      telegram: '+70987654321'
    )
    assert membership_request.valid?
  end

  def test_two_membership_requests_with_blank_email
    email = ''
    create(
      :membership_request,
      full_name: 'Some name',
      email: email,
      phone: '+71234567890',
      telegram: '+71234567890'
    )
    membership_request = build(
      :membership_request,
      full_name: 'Other name',
      email: email,
      phone: '+70987654321',
      telegram: '+70987654321'
    )
    assert membership_request.valid?
  end

  def test_if_email_with_different_letter_case_will_do
    email = 'some@email.example'
    create(
      :membership_request,
      full_name: 'Some name',
      email: email.upcase,
      phone: '+71234567890',
      telegram: '+71234567890'
    )
    membership_request = build(
      :membership_request,
      full_name: 'Other name',
      email: email,
      phone: '+70987654321',
      telegram: '+70987654321'
    )
    refute membership_request.valid?
  end

  def test_maybe_duplicate_phone_is_accepted
    phone = '+71234567890'
    create(
      :membership_request,
      full_name: 'Some name',
      email: 'some@email.example',
      phone: phone,
      telegram: '+71234567890'
    )
    membership_request = build(
      :membership_request,
      full_name: 'Other name',
      email: 'other@email.example',
      phone: phone,
      telegram: '+70987654321'
    )
    refute membership_request.valid?
  end

  def test_same_phone_after_fifteen_minutes
    phone = '+71234567890'
    create(
      :membership_request,
      full_name: 'Some name',
      email: 'some@email.example',
      phone: phone,
      telegram: '+71234567890',
      created_at: 15.minutes.ago
    )
    membership_request = build(
      :membership_request,
      full_name: 'Other name',
      email: 'other@email.example',
      phone: phone,
      telegram: '+70987654321'
    )
    assert membership_request.valid?
  end

  def test_membership_request_with_existing_telegram
    telegram = '+71234567890'
    create(
      :membership_request,
      full_name: 'Some name',
      email: 'some@email.example',
      phone: '+71234567890',
      telegram: telegram
    )
    membership_request = build(
      :membership_request,
      full_name: 'Other name',
      email: 'other@email.example',
      phone: '+70987654321',
      telegram: telegram
    )
    refute membership_request.valid?
  end

  def test_duplicate_telegram_fifteen_minutes_later
    telegram = '+71234567890'
    create(
      :membership_request,
      full_name: 'Some name',
      email: 'some@email.example',
      phone: '+71234567890',
      telegram: telegram,
      created_at: 15.minutes.ago
    )
    membership_request = build(
      :membership_request,
      full_name: 'Other name',
      email: 'other@email.example',
      phone: '+70987654321',
      telegram: telegram
    )
    assert membership_request.valid?
  end

  def test_blank_telegram_twice_and_everything_is_valid
    telegram = ''
    create(
      :membership_request,
      full_name: 'Some name',
      email: 'some@email.example',
      phone: '+71234567890',
      telegram: telegram
    )
    membership_request = build(
      :membership_request,
      full_name: 'Other name',
      email: 'other@email.example',
      phone: '+70987654321',
      telegram: telegram
    )
    assert membership_request.valid?
  end

  def test_if_upper_and_lower_case_telegram_may_coexist
    telegram = 'telegram'
    create(
      :membership_request,
      full_name: 'Some name',
      email: 'some@email.example',
      phone: '+71234567890',
      telegram: telegram.upcase
    )
    membership_request = build(
      :membership_request,
      full_name: 'Other name',
      email: 'other@email.example',
      phone: '+70987654321',
      telegram: telegram
    )
    refute membership_request.valid?
  end

  def test_if_comment_has_leading_or_trailing_spaces
    comment = ' Some comment  '
    membership_request = build(:membership_request, comment: comment)
    membership_request.validate
    assert_equal comment.strip, membership_request.comment
  end

  def test_spaces_around_district
    district = ' Some district '
    membership_request = build(:membership_request, district: district)
    membership_request.validate
    assert_equal district.strip, membership_request.district
  end

  def test_if_spaces_in_email_are_stripped
    email = ' some@email.example '
    membership_request = build(:membership_request, email: email)
    membership_request.validate
    assert_equal email.strip, membership_request.email
  end

  def test_that_spaces_in_full_name_are_removed
    full_name = ' Some name '
    membership_request = build(:membership_request, full_name: full_name)
    membership_request.validate
    assert_equal full_name.strip, membership_request.full_name
  end

  def test_that_phone_contains_no_whitespace_around_it
    phone = ' +71234567890 '
    membership_request = build(:membership_request, phone: phone)
    membership_request.validate
    assert_equal phone.strip, membership_request.phone
  end

  def test_what_about_spaces_in_occupation
    occupation = ' Some occupation '
    membership_request = build(:membership_request, occupation: occupation)
    membership_request.validate
    assert_equal occupation.strip, membership_request.occupation
  end

  def test_if_spaces_in_public_organizations_are_stripped
    organizations = ' Such and such organizations '
    membership_request = build(
      :membership_request,
      public_organizations: organizations
    )

    membership_request.validate
    assert_equal organizations.strip, membership_request.public_organizations
  end

  def test_spaces_in_residence
    residence = ' Some city '
    membership_request = build(:membership_request, residence: residence)
    membership_request.validate
    assert_equal residence.strip, membership_request.residence
  end

  def test_if_telegram_has_leading_or_trailing_spaces
    telegram = ' telegram '
    membership_request = build(:membership_request, telegram: telegram)
    membership_request.validate
    assert_equal telegram.strip, membership_request.telegram
  end

  def test_that_telegram_is_normalized
    membership_request = build(
      :membership_request,
      telegram: 'https://t.me/telegram'
    )
    membership_request.validate
    assert_equal 'telegram', membership_request.telegram
  end
end
