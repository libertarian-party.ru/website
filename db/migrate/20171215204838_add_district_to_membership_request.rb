# frozen_string_literal: true

class AddDistrictToMembershipRequest < ActiveRecord::Migration[5.1]
  def change
    add_column :membership_requests, :district, :string
  end
end
