import { withBemMod } from '@bem-react/core'

import { cnHeading } from '../Heading'
import './Heading_size_s.css'

export const HeadingSizeS = withBemMod(cnHeading(), { size: 's' })
