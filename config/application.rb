# frozen_string_literal: true

require_relative 'boot'
require 'rails/all'

Bundler.require(*Rails.groups)
Dotenv.load('.env') if Rails.env.in?(%w[development test])

module PartyWebsite
  class Application < Rails::Application
    config.load_defaults 5.2
  end
end
