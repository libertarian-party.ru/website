# frozen_string_literal: true

class RenameNewsToPublications < ActiveRecord::Migration[5.1]
  def change
    rename_table :news, :publications

    rename_table :images_news, :images_publications
    rename_column :images_publications, :news_id, :publication_id
  end
end
