import React from 'react'
import { shallow } from 'enzyme'

import { ContactLink } from '../-Link/Contact-Link'
import { ContactEmail } from './Contact-Email'

describe('Contact-Email', () => {
  const contactEmail = shallow(<ContactEmail />)

  it('is Contact-Link', () => {
    expect(contactEmail.is(ContactLink)).toBe(true)
  })

  it('has class Contact-Email', () => {
    expect(contactEmail.prop('className')).toBe('Contact-Email')
  })

  it('includes no children by default', () => {
    expect(contactEmail.children().length).toBe(0)
  })
})

describe('Contact-Email with a child', () => {
  const contactEmail = shallow(<ContactEmail>hello@email.example</ContactEmail>)

  it('contains the child', () => {
    expect(contactEmail.children().length).toBe(1)
    expect(contactEmail.render().text()).toBe('hello@email.example')
  })

  it('copies the child to href, adding "mailto:" to the beginning', () => {
    expect(contactEmail.prop('href')).toBe('mailto:hello@email.example')
  })
})
