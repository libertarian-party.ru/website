# frozen_string_literal: true

class CreateMembershipRequests < ActiveRecord::Migration[5.1]
  def change
    create_table :membership_requests, id: :uuid do |t|
      t.string :full_name
      t.date :birthdate
      t.string :phone
      t.references :federal_subject, type: :uuid
      t.string :residence
      t.string :email
      t.string :telegram
      t.string :profession
      t.text :public_organizations
      t.text :comment

      t.timestamps
    end
  end
end
