# frozen_string_literal: true

class RemoveNotNullConstraints < ActiveRecord::Migration[5.2]
  def change
    change_column_null :federal_subjects, :created_at, true
    change_column_null :federal_subjects, :updated_at, true

    change_column_null :illustrations, :created_at, true
    change_column_null :illustrations, :updated_at, true

    change_column_null :invites, :created_at, true
    change_column_null :invites, :updated_at, true

    change_column_null :membership_requests, :created_at, true
    change_column_null :membership_requests, :updated_at, true

    change_column_null :pictures, :created_at, true
    change_column_null :pictures, :updated_at, true

    change_column_null :publications, :created_at, true
    change_column_null :publications, :updated_at, true

    change_column_null :users, :created_at, true
    change_column_null :users, :updated_at, true
  end
end
