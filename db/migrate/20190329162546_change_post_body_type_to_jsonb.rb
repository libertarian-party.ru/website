# frozen_string_literal: true

class ChangePostBodyTypeToJsonb < ActiveRecord::Migration[5.2]
  def change
    change_column :posts, :body, 'jsonb USING body::jsonb'
  end
end
