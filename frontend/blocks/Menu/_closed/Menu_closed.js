import React from 'react'
import { withBemMod } from '@bem-react/core'

import { cnMenu } from '../Menu'
import './Menu_closed.css'

export const MenuClosed = withBemMod(cnMenu(), { closed: true })
