# frozen_string_literal: true

class RenameProfessionToOccupation < ActiveRecord::Migration[5.1]
  def change
    rename_column :membership_requests, :profession, :occupation
  end
end
