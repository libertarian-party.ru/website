document.addEventListener("turbolinks:load", function () {
  var showButtons = Util.selectAll("[data-show]");

  if (showButtons.length > 0) {
    document.addEventListener("click", function (event) {
      if (showButtons.includes(event.target) && Util.isPrimaryButton(event)) {
        var elements = Util.selectAll(event.target.getAttribute("data-show"));
        elements.forEach(function (el) {
          Util.show(el);
        });
      }
    });
  }
});
