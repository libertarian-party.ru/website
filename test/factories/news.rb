# frozen_string_literal: true

FactoryBot.define do
  factory :news do
    association :author, factory: :user

    trait :published do
      association :thumbnail, factory: :picture
      annotation 'Информация обновляется и дополняется'
      body '{"blocks": [{"text": "В субботу по всей стране прошли протестные акции оппозиции…"}], "entityMap": {}}'
      published true
      published_at { Date.yesterday }
      title 'Митинги 5-го мая'
    end
  end
end
