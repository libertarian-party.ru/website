import React from 'react'
import { shallow } from 'enzyme'

import { BaseFooterChatLink } from '../Footer-ChatLink'
import { FooterChatLinkMobile } from './Footer-ChatLink_mobile'

const FooterChatLink = FooterChatLinkMobile(BaseFooterChatLink)

describe('Footer-ChatLink_mobile', () => {
  const footerChatLink = shallow(<FooterChatLink mobile />)

  it('has classes Footer-ChatLink and Footer-ChatLink_mobile', () => {
    expect(footerChatLink.prop('className'))
      .toBe('Footer-ChatLink Footer-ChatLink_mobile')
  })
})
