# frozen_string_literal: true

FactoryBot.define do
  factory :article do
    association :author, factory: :user

    trait :published do
      association :thumbnail, factory: :picture
      annotation 'Основные тезисы, история развития и политическое влияние'
      body '{"blocks": [{"text": "В романах «Источник» и «Атлант расправил плечи» Рэнд вывела…"}], "entityMap": {}}'
      published true
      published_at { Date.yesterday }
      title 'Объективизм по Айн Рэнд'
    end
  end
end
