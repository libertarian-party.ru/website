# frozen_string_literal: true

module Admin
  class SessionsController < BaseController
    skip_before_action :require_login, except: :destroy

    def new
      redirect_to admin_root_url if logged_in?
    end

    def create
      if login(params[:email], params[:password], params[:remember])
        redirect_back_or_to admin_root_url, notice: t('.success')
      else
        flash.now[:alert] = t('.incorrect_credentials')
        render :new
      end
    end

    def destroy
      logout
      redirect_to login_url, notice: t('.success')
    end
  end
end
