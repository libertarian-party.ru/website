import { withBemMod } from '@bem-react/core'

import { cnHome } from '../../Home'
import './Home-Tabs_mobile.css'

export const HomeTabsMobile = withBemMod(cnHome('Tabs'), { mobile: true })
