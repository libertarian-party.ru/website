# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def test_that_factory_defines_valid_user
    user = build(:user)
    assert user.valid?
  end

  def test_if_user_is_valid_without_a_name
    user = build(:user, name: '')
    refute user.valid?
  end

  def test_user_with_existing_name
    existing_user = create(:user, name: 'Some Name')
    user = build(:user, name: existing_user.name)
    refute user.valid?
  end

  def test_email_presence_validation
    user = build(:user, email: '')
    refute user.valid?
  end

  def test_that_email_is_downcased
    user = build(:user, email: 'Email@Example.com')
    user.validate
    assert_equal 'email@example.com', user.email
  end

  def test_that_not_unique_email_is_rejected
    existing_user = create(:user, email: 'some@email.example')
    user = build(:user, email: existing_user.email.capitalize)
    refute user.valid?
  end

  def test_too_short_password
    user = build(:user, password: 'foo')
    refute user.valid?
  end

  def test_if_blank_password_confirmation_is_okay
    user = build(:user)

    user.password_confirmation = ''
    refute user.valid?

    user.password_confirmation = nil
    refute user.valid?
  end

  def test_what_if_passwords_do_not_match
    user = build(:user)
    user.password_confirmation = user.password.reverse
    refute user.valid?
  end

  def test_that_invite_is_required
    user = build(:user, invite: nil)
    refute user.valid?
  end

  def test_that_two_users_cannot_share_one_invite
    invite = build(:invite)
    create(:user, invite: invite)

    user = build(:user, invite: invite)
    refute user.valid?
  end

  def test_that_admin_is_always_false
    user = build(:user, admin: true)
    user.validate
    refute user.admin?
  end
end
