# frozen_string_literal: true

require 'test_helper'

class NewsTest < ActiveSupport::TestCase
  def setup
    @news = News.new
  end

  def test_no_predefined_author
    assert_nil @news.author
  end

  def test_type_is_news
    assert_equal 'News', @news.type
  end

  def test_published_eh
    refute @news.published?
  end
end

class NewsSaveTest < ActiveSupport::TestCase
  def setup
    @news = build(:news)
  end

  def test_save
    assert @news.save
  end

  def test_without_author
    @news.author = nil
    refute @news.save
  end
end

class PublishedNewsTest < ActiveSupport::TestCase
  def setup
    @news = build(:news, :published)
  end

  def test_published_eh
    assert @news.published?
  end
end

class PublishedNewsSaveTest < ActiveSupport::TestCase
  def setup
    @news = build(:news, :published)
  end

  def test_save
    assert @news.save
  end

  def test_with_blank_annotation
    [nil, '', ' '].each do |blank_value|
      @news.annotation = blank_value
      refute @news.save
    end
  end

  def test_with_too_long_annotation
    @news.annotation = 'A' * 141
    refute @news.save
  end

  def test_with_blank_body
    [nil, '', ' '].each do |blank_value|
      @news.body = blank_value
      refute @news.save
    end
  end

  def test_without_published_at
    @news.published_at = nil
    refute @news.save
  end

  def test_without_thumbnail
    @news.thumbnail = nil
    refute @news.save
  end

  def test_with_blank_title
    [nil, '', ' '].each do |blank_value|
      @news.title = blank_value
      refute @news.save
    end
  end
end
