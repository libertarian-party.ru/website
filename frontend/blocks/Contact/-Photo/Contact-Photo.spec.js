import React from 'react'
import { shallow } from 'enzyme'

import { ContactPhoto } from './Contact-Photo'

describe('Contact-Photo', () => {
  const contactPhoto = shallow(<ContactPhoto />)

  it('is <img>', () => {
    expect(contactPhoto.is('img')).toBe(true)
  })

  it('has class Contact-Photo', () => {
    expect(contactPhoto.prop('className')).toBe('Contact-Photo')
  })

  it('contains blank alt', () => {
    expect(contactPhoto.prop('alt')).toBe('')
  })

  it('has src undefined', () => {
    expect(contactPhoto.prop('src')).toBe(undefined)
  })

  it('has undefined srcSet', () => {
    expect(contactPhoto.prop('srcSet')).toBe(undefined)
  })
})

describe('Contact-Photo with src', () => {
  const contactPhoto = shallow(<ContactPhoto src='pic.jpg' />)

  it('passes that to <img> as src', () => {
    expect(contactPhoto.prop('src')).toBe('pic.jpg')
  })
})

describe('Contact-Photo with srcSet', () => {
  const contactPhoto = shallow(<ContactPhoto srcSet='pic@2x.jpg 2x' />)

  it('passes that srcSet to inner <img>', () => {
    expect(contactPhoto.prop('srcSet')).toBe('pic@2x.jpg 2x')
  })
})
