import React from 'react'

import Contact from '../../Contact'
import Contacts from '../../Contacts'
import Link from '../../Link'

import { cnHome } from '../Home'

import akater from '../images/akater.jpg'
import akater2x from '../images/akater@2x.jpg'
import boiko from '../images/boiko.jpg'   
import boiko2x from '../images/boiko@2x.jpg'
import kalyonov from '../images/kalyonov.jpg'
import kalyonov2x from '../images/kalyonov@2x.jpg'                                                       
import kichanova from '../images/kichanova.jpg'
import kichanova2x from '../images/kichanova@2x.jpg' 
import kuznetsova from '../images/kuznetsova.jpg'
import kuznetsova2x from '../images/kuznetsova@2x.jpg'
//import maximov from '../images/maximov.jpg'
//import maximov2x from '../images/maximov@2x.jpg'  
import nozdrin from '../images/nozdrin.jpg'
import nozdrin2x from '../images/nozdrin@2x.jpg'
import osenin from '../images/osenin.jpg'
import osenin2x from '../images/osenin@2x.jpg'
import ovsienko from '../images/ovsienko.jpg'
import ovsienko2x from '../images/ovsienko@2x.jpg'
import prohorov from '../images/prohorov.jpg'
import prohorov2x from '../images/prohorov@2x.jpg'
import samodurov from '../images/samodurov.jpg'
import samodurov2x from '../images/samodurov@2x.jpg' 
import svetov from '../images/svetov.jpg'
import svetov2x from '../images/svetov@2x.jpg'
import tyulenin from '../images/tyulenin.jpg'
import tyulenin2x from '../images/tyulenin@2x.jpg'
import conway from '../images/conway.jpg'
import conway2x from '../images/conway.jpg'

import romanov from '../images/romanov.jpg'
import romanov2x from '../images/romanov.jpg'
import grach from '../images/grach.jpg'
import grach2x from '../images/grach.jpg'
import maximov from '../images/maximov.jpg'
import maximov2x from '../images/maximov.jpg'
import chinarov from '../images/chinarov.jpg'
import chinarov2x from '../images/chinarov.jpg'
import rina from '../images/rina.jpg'
import rina2x from '../images/rina.jpg'                                                                  
import tolkien from '../images/tolkien.jpg'
import tolkien2x from '../images/tolkien.jpg'
import mariaivo from '../images/mariaivo.jpg'
import mariaivo2x from '../images/mariaivo.jpg'
import delugov from '../images/delugov.jpg'
import delugov2x from '../images/delugov.jpg'                                                            
import marat from '../images/marat.jpg'
import marat2x from '../images/marat.jpg'
import placeholder from '../images/placeholder.svg'
import placeholder2x from '../images/placeholder.svg'

import barykin from '../images/barykin.jpg'
import barykin2x from '../images/barykin@2x.jpg'
import sheinin from '../images/sheinin.jpg'
import sheinin2x from '../images/sheinin@2x.jpg'
import seversky from '../images/seversky.jpg'
import seversky2x from '../images/seversky@2x.jpg'

export const HomeControlCommission = () => (
  <Contacts
    desc={
      <React.Fragment>
        контролирует соблюдение устава, исполнение решений руководящих
        органов партии, а&nbsp;также контролирует финансовую, хозяйственную
        деятельность партии и&nbsp;её региональных отделений. {}
        <Link href='mailto:ckrk@libertarian-party.ru' theme='default'>
          ckrk@libertarian&#8209;party.ru
        </Link>
      </React.Fragment>
    }
    title='Центральная контрольно-ревизионная комиссия'
    withLabel>
    <Contact
      location='Саратов'
      name='Никита Барыкин'
      photo={barykin}
      photo2x={barykin2x}
      position='Центральная контрольно-ревизионная комиссия' />
    <Contact
      location='Женева'
      name='Артём Северский'
      photo={seversky}
      photo2x={seversky2x}
      position='Центральная контрольно-ревизионная комиссия' />
    <Contact
      location='Ярославль'
      name='Антон Шейнин'
      photo={sheinin}
      photo2x={sheinin2x}
      position='Центральная контрольно-ревизионная комиссия' />
  </Contacts>
)
