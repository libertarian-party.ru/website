import React from 'react'
import { shallow } from 'enzyme'

import Close from '../../Close'
import Logo from '../../Logo'
import { MenuHeader } from './Menu-Header'

describe('Menu-Header', () => {
  const menuHeader = shallow(<MenuHeader />)

  it('is <div>', () => {
    expect(menuHeader.is('div')).toBe(true)
  })

  it('has class Menu-Header', () => {
    expect(menuHeader.prop('className')).toBe('Menu-Header')
  })

  it('contains Close', () => {
    expect(menuHeader.contains(<Close />)).toBe(true)
  })

  it('contains Logo with default theme and text "ЛПР"', () => {
    const logo = <Logo theme='default'>ЛПР</Logo>
    expect(menuHeader.contains(logo)).toBe(true)
  })
})

describe('Menu-Header with onCloseClick function', () => {
  const closeClickFunc = () => 'just some function'
  const menuHeader = shallow(<MenuHeader onCloseClick={closeClickFunc} />)

  it('passes that function to Close as onClick', () => {
    expect(menuHeader.contains(<Close onClick={closeClickFunc} />)).toBe(true)
  })
})
