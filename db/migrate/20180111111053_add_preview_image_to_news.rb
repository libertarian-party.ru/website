# frozen_string_literal: true

class AddPreviewImageToNews < ActiveRecord::Migration[5.1]
  def change
    add_column :news, :preview_image_fingerprint, :string
    add_attachment :news, :preview_image
  end
end
