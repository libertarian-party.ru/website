import React from 'react'
import { shallow } from 'enzyme'

import { Heading as BaseHeading } from '../Heading'
import { HeadingSizeS } from './Heading_size_s'

const Heading = HeadingSizeS(BaseHeading)

describe('Heading_size_s', () => {
  const headingSizeS = shallow(<Heading size='s' />)

  it('has classes Heading and Heading_size_s', () => {
    expect(headingSizeS.prop('className')).toBe('Heading Heading_size_s')
  })
})
