import { withBemMod } from '@bem-react/core'

import { cnButton } from '../Button'
import './Button_theme_secondary.css'

export const ButtonThemeSecondary = withBemMod(
  cnButton(),
  { theme: 'secondary' }
)
