import React from 'react'
import { withBemMod } from '@bem-react/core'

import Contact from '../../../Contact'
import Contacts from '../../../Contacts'
import Heading from '../../../Heading'
import Paragraph from '../../../Paragraph'
import Symbols from '../../../Symbols'

import { PageContent } from '../../-Content/Page-Content'
import { PageFooter } from '../../-Footer/Page-Footer'
import { PageHeader } from '../../-Header/Page-Header'
import { cnPage } from '../../Page'

import goode from './photos/goode.jpg'
import goode2x from './photos/goode@2x.jpg'
import boiko from './photos/boiko.jpg'
import boiko2x from './photos/boiko@2x.jpg'
import svetov from './photos/svetov.jpg'
import svetov2x from './photos/svetov@2x.jpg'
import dari from './photos/dari.png'
import dari2x from './photos/dari@2x.png'
import fedyukin from './photos/fedyukin.jpg'
import fedyukin2x from './photos/fedyukin@2x.jpg'
import './Page_name_press.css'

const newBody = (Base, props) => (
  <Base {...props} >
    <PageHeader theme='default' activeLink='party' />
    <PageContent tag='main'>
      <Heading className={cnPage('Head')} size='l' tag='h1'>
        Для прессы
      </Heading>

      <Heading className={cnPage('Sub')} size='s' tag='h2'>
        Пресс-секретарь и спикеры
      </Heading>
      <Contacts className={cnPage('Contacts')}>
        <Contact
          name='Дарья Петрушенко'
          phone='+7 977 404-21-99'
          photo={dari}
          photo2x={dari2x}
          position='И.О. пресс-секретаря'
          telegram='DariFY13' />
        <Contact
          name='Борис Федюкин'
	  phone=''
          photo={fedyukin}
          photo2x={fedyukin2x}
          position='Председатель'
          telegram='borisfedyukin' />
      </Contacts>

      <div className={cnPage('Dyad')}>
        <div className={cnPage('AboutParty')}>
          <Heading className={cnPage('Sub')} size='s' tag='h2'>
            Коротко о партии
          </Heading>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Либертарианская партия России (ЛПР) возникла в 2008 году, и на апрель 2019‑го в ней состоит 850 членов и 250 сторонников из 72 регионов России. Мы не лидерская, а идеологическая партия: нас связывают не конкретные личности, а общие идеи.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Либертарианство — это система ценностей, постулирующая свободу, основанную на отсутствии агрессии и принуждения. Либертарианцы выступают за максимизацию личных и экономических свобод и минимизацию влияния государства на жизнь общества.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Члены партии были избраны в&nbsp;муниципальные депутаты (2012,
            2014, 2017) в&nbsp;Москве и&nbsp;Подмосковье. Мы&nbsp;регулярно
            участвуем в&nbsp;муниципальных выборах, а&nbsp;также участвовали
            в&nbsp;выборах в&nbsp;Московскую Городскую Думу,
            в&nbsp;Государственную Думу, в&nbsp;региональных выборах мэра
            и&nbsp;в&nbsp;Законодательное Собрание.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
	    Мы регулярно проводим лекции, дебаты, участвуем в Форумах Свободных Людей, Чтениях Адама Смита, Мемориальных Конференциях Айн Рэнд. Организовываем митинги, в том числе против блокировки Телеграма 30 апреля 2018, против пенсионной реформы и налогового грабежа 29 июля 2018.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
	    Мы не сотрудничаем с системной оппозицией. Государство отказывается регистрировать нашу партию, мы пытались четыре раза, последний в 2015 году. Наша цель — изменение существующей модели государственного устройства России мирными средствами в соответствии с либертарианскими ценностями.
          </Paragraph>
        </div>

        <div className={cnPage('Other')}>
          <Heading className={cnPage('Sub')} size='s' tag='h2'>
            Символика
          </Heading>
          <Symbols />
        </div>
      </div>
    </PageContent>
    <PageFooter isPressButtonDisabled />
  </Base>
)

export const PageNamePress = withBemMod(
  cnPage(),
  { name: 'press' },
  newBody
)
