# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def reset_password(user)
    @user = user
    @user.generate_reset_password_token!

    @password_reset_uri =
      edit_admin_user_password_url(user, token: @user.reset_password_token)

    mail(to: @user.email)
  end
end
