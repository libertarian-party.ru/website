import React from 'react'
import { shallow } from 'enzyme'

import { Social as BaseSocial } from '../Social'
import { SocialSizeM } from './Social_size_m'

const Social = SocialSizeM(BaseSocial)

describe('Social_size_m', () => {
  const socialSizeM = shallow(<Social size='m' />)

  it('has classes Social and Social_size_m', () => {
    expect(socialSizeM.prop('className')).toBe('Social Social_size_m')
  })
})
