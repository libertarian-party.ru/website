import React from 'react'
import { shallow } from 'enzyme'

import { BaseLogoIcon as LogoIcon } from './Logo-Icon'

describe('Logo-Icon', () => {
  const logoIcon = shallow(<LogoIcon />)

  it('is <span>', () => {
    expect(logoIcon.is('span')).toBe(true)
  })

  it('has class Logo-Icon by default', () => {
    expect(logoIcon.prop('className')).toBe('Logo-Icon')
  })
})

describe('Logo-Icon with some class', () => {
  const logoIcon = shallow(<LogoIcon className='Some-Class' />)

  it('now has one more class', () => {
    expect(logoIcon.prop('className')).toBe('Logo-Icon Some-Class')
  })
})
