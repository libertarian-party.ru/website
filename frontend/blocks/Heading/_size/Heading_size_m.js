import { withBemMod } from '@bem-react/core'

import { cnHeading } from '../Heading'
import './Heading_size_m.css'

export const HeadingSizeM = withBemMod(cnHeading(), { size: 'm' })
