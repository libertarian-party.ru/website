# frozen_string_literal: true

class CreateInvites < ActiveRecord::Migration[5.1]
  def change
    create_table :invites, id: :uuid do |t|
      t.string :comment
      t.timestamps
    end

    add_reference :users, :invite, type: :uuid
  end
end
