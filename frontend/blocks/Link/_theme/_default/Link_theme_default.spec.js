import React from 'react'
import { shallow } from 'enzyme'

import { Link as BaseLink } from '../../Link'
import { LinkThemeDefault } from './Link_theme_default'

const Link = LinkThemeDefault(BaseLink)

describe('Link_theme_default', () => {
  const linkThemeDefault = shallow(<Link theme='default' />)

  it('has classes Link and Link_theme_default', () => {
    expect(linkThemeDefault.prop('className')).toBe('Link Link_theme_default')
  })
})
