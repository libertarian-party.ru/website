# frozen_string_literal: true

class CreateRegionalBranches < ActiveRecord::Migration[5.2]
  def change
    create_table :regional_branches, id: :uuid do |t|
      t.belongs_to :federal_subject, type: :uuid
      t.string :telegram_chat
      t.string :trello_list
      t.timestamps null: true
    end
  end
end
