# Официальный сайт Либертарианской партии России

## Сообщить о баге
[Начните обсуждение][new_issue]. Опишите шаги, которые привели к проблеме.
Прикрепите скриншоты. Чем больше вы предоставите информации, тем быстрее мы
всё починим. Потребуется зарегистрироваться либо войти через Google, Twitter
или GitHub.

## Поучаствовать в разработке
Помощь приветствуется, присылайте патчи :-)

## Установка
Установить следующие пакеты:
* Ruby (требуется версия ruby ~> 2.5.3). Рекомендуется использовать RVM (https://rvm.io/rvm/install):
  * gpg2 --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
  * curl -sSL https://get.rvm.io | bash -s stable --ruby
  * usermod -a -G rvm root (либо другой пользователь, из под которого будет запускаться приложение)
  * source /usr/local/rvm/scripts/rvm
  * rvm install "ruby-2.5.1"
  * rvm use 2.5.1
* NPM
* Yarn (npm install yarn -g)
* ImageMagick
* PostgreSQL
  * Database
  * libpq-devel



## Deploy
# Production
Раздел ниже нерабочий и неактуальный, переписать попозже.

Скопировать файл config/examples/dotenv в корневую
директорию проекта с именем .env и при необходимости изменить его
содержимое — указать настройки для подключения к БД и переменные окружения.
```
$ git clone git@gitlab.com:libertarian-party.ru/website.git
$ cd website/
$ cp config/examples/dotenv .env
```

Выполнить скрипт для инициализации проекта.
```
$ scripts/init
```

# Development
Скопируйте репозиторий и исполните setup-скрипт.
```
$ git clone git@gitlab.com:libertarian-party/party_website.git
$ cd party_website/
$ bin/rake project:setup
```

## Запуск
В ручном режиме достаточно выполнить скрипт scripts/run.sh
В директории scripts также представлен service-файл для systemd. Для его
использования необходимо скопировать файл в директорию systemd и поправить содержимое,
указав путь до директории приложения.
```
$ cp scripts/lpr-app.service /etc/systemd/system/lpr-app.service
$ vi /etc/systemd/system/lpr-app.service
$ systemctl start lpr-app
$ systemctl status lpr-app
```

## Веб-сервер
Пример конфигурационного файла для nginx представлен в config/examples/nginx-app.conf

## Настройки                                                                                                                                                                                                       
Приложение конфигурируется при помощи переменных среды, достаточно указать их в файле .env и /scripts/run.sh.
Их перечень с пояснениями:

RAILS_ENV
Окружение Ruby on Rails (development, test, production).

NODE_ENV
Окружение Node.js, следует указывать только значение production.

PORT
Номер порта сервера приложений puma.

WEB_CONCURRENCY
Число процессов puma.

RAILS_MIN_THREADS
Минимальное число потоков для каждого процесса.

RAILS_MAX_THREADS
Максимальное число потоков для каждого процесса.
Общее количество потоков (WEB_CONCURRENCY * RAILS_MAX_THREADS) не должно
превышать количество доступных подключений к БД.

DATABASE_URL
URI для подключения к БД.

SECRET_KEY_BASE
Ключ для шифрования и подписи данных сессии, хранимых в cookie.
Может быть сгенерирован при помощи `$ bin/rails secret` в директории
приложения или `$ openssl rand -hex 64`.

RAILS_LOG_TO_STDOUT
Направить логи в stdout, а не в файл. Если переменная объявлена, любое
значение трактуется как true.

TRELLO_DEVELOPER_PUBLIC_KEY
Публичный ключ, идентифицирующий приложение/разработчика.

TRELLO_MEMBER_TOKEN
Токен, авторизующий приложение на чтение досок, добавление карточек и т.д.

TRELLO_FEDERAL_LIST
Федеральная Trello-доска для работы с заявками.

TELEGRAM_TOKEN
Токен для аутентификации Telegram-бота.

TELEGRAM_FEDERAL_CHAT
Федеральный Telegram-чат для работы с заявками.

[new_issue]: https://gitlab.com/libertarian-party.ru/website/issues/new
