import React from 'react'
import { shallow } from 'enzyme'

import { BaseHeaderBurger } from '../Header-Burger'
import { HeaderBurgerThemeNegative } from './Header-Burger_theme_negative'
import { BurgerIcon } from './icons/Burger'

const HeaderBurger = HeaderBurgerThemeNegative(BaseHeaderBurger)

describe('Header-Burger_theme_negative', () => {
  const headerBurgerThemeNegative = shallow(<HeaderBurger theme='negative' />)

  it('has classes Header-Burger and Header-Burger_theme_negative', () => {
    expect(headerBurgerThemeNegative.prop('className'))
      .toBe('Header-Burger Header-Burger_theme_negative')
  })

  it('contains burger icon', () => {
    expect(headerBurgerThemeNegative.prop('icon')).toMatchObject(<BurgerIcon />)
  })
})
