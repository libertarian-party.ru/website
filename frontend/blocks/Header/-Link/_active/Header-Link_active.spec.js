import React from 'react'
import { shallow } from 'enzyme'

import { BaseHeaderLink } from '../Header-Link'
import { HeaderLinkActive } from './Header-Link_active'

const HeaderLink = HeaderLinkActive(BaseHeaderLink)

describe('Header-Link_active', () => {
  const headerLinkActive = shallow(<HeaderLink active />)

  it('has classes Header-Link and Header-Link_active', () => {
    expect(headerLinkActive.prop('className'))
      .toBe('Header-Link Header-Link_active')
  })
})
