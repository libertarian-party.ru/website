import React from 'react'
import { shallow } from 'enzyme'

import { QuoteSource } from './Quote-Source'

describe('Quote-Source', () => {
  const quoteSource = shallow(<QuoteSource />)

  it('is <figcaption>', () => {
    expect(quoteSource.is('figcaption')).toBe(true)
  })

  it('has class Quote-Source', () => {
    expect(quoteSource.prop('className')).toBe('Quote-Source')
  })

  it('is blank by default', () => {
    expect(quoteSource.children().length).toBe(0)
  })
})

describe('Quote-Source with children', () => {
  const quoteSource = shallow(
    <QuoteSource>
      <cite>Book Title</cite>
    </QuoteSource>
  )

  it('contains the children', () => {
    expect(quoteSource.children().length).toBe(1)
    expect(quoteSource.contains(<cite>Book Title</cite>)).toBe(true)
  })
})
