import React from 'react'
import { shallow } from 'enzyme'

import { BaseHeaderBurger as HeaderBurger } from './Header-Burger'

describe('Header-Burger', () => {
  const headerBurger = shallow(<HeaderBurger />)

  it('is <button>', () => {
    expect(headerBurger.is('button')).toBe(true)
  })

  it('has class Header-Burger by default', () => {
    expect(headerBurger.prop('className')).toBe('Header-Burger')
  })

  it('has no children by default', () => {
    expect(headerBurger.children().length).toBe(0)
  })

  it('has undefined onClick by default', () => {
    expect(headerBurger.prop('onClick')).toBeUndefined()
  })
})

describe('Header-Burger with additional class', () => {
  const headerBurger = shallow(<HeaderBurger className='Abc' />)

  it('adds that class to the default one', () => {
    expect(headerBurger.prop('className')).toBe('Header-Burger Abc')
  })
})

describe('Header-Burger with icon', () => {
  const headerBurger = shallow(<HeaderBurger icon={<svg />} />)

  it('includes the icon as a child', () => {
    expect(headerBurger.children().length).toBe(1)
    expect(headerBurger.contains(<svg />)).toBe(true)
  })
})

describe('Header-Burger with onClick function', () => {
  const onClickFunc = jest.fn()
  const headerBurger = shallow(<HeaderBurger onClick={onClickFunc} />)

  it('executes that function on click', () => {
    headerBurger.simulate('click')
    expect(onClickFunc).toBeCalled()
  })
})
