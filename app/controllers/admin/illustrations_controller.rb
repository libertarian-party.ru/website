# frozen_string_literal: true

module Admin
  class IllustrationsController < BaseController
    def create
      illus = Illustration.new(illus_params)

      if illus.save
        render json: {uri: url_for(illus.picture.file)}
      else
        render json: illus, status: :unprocessable_entity
      end
    end

    private

    def illus_params
      params
        .require(:illustration)
        .permit(
          :alt,
          :post_id,
          picture_attributes: %i[file]
        )
    end
  end
end
