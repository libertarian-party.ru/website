import React from 'react'
import { shallow } from 'enzyme'

import { Social as BaseSocial } from '../Social'
import { SocialMediumVk } from './Social_medium_vk'

const Social = SocialMediumVk(BaseSocial)

describe('Social_medium_vk', () => {
  const socialMediumVk = shallow(<Social medium='vk' />)

  it('has classes Social and Social_medium_vk', () => {
    expect(socialMediumVk.prop('className'))
      .toBe('Social Social_medium_vk')
  })

  it('contains an icon', () => {
    expect(socialMediumVk.prop('icon')).toBeTruthy()
  })
})
