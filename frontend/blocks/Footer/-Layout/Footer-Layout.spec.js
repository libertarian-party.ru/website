import React from 'react'
import { shallow } from 'enzyme'

import { FooterLayout } from './Footer-Layout'

describe('Footer-Layout', () => {
  const footerLayout = shallow(<FooterLayout />)

  it('is <div>', () => {
    expect(footerLayout.is('div')).toBe(true)
  })

  it('has class Footer-Layout', () => {
    expect(footerLayout.prop('className')).toBe('Footer-Layout')
  })

  it('has no children by default', () => {
    expect(footerLayout.children().length).toBe(0)
  })
})

describe('Footer-Layout with a child', () => {
  const footerLayout = shallow(<FooterLayout><div /></FooterLayout>)

  it('now contains a child', () => {
    expect(footerLayout.children().length).toBe(1)
    expect(footerLayout.contains(<div />)).toBe(true)
  })
})
