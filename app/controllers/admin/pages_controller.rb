# frozen_string_literal: true

module Admin
  class PagesController < BaseController
    def home
      redirect_to admin_posts_url
    end
  end
end
