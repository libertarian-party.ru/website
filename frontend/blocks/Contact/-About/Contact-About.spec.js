import React from 'react'
import { shallow } from 'enzyme'

import { ContactAbout } from './Contact-About'

describe('Contact-About', () => {
  const contactAbout = shallow(<ContactAbout />)

  it('is <p>', () => {
    expect(contactAbout.is('p')).toBe(true)
  })

  it('has class Contact-About', () => {
    expect(contactAbout.prop('className')).toBe('Contact-About')
  })

  it('has no children by default', () => {
    expect(contactAbout.children().length).toBe(0)
  })
})

describe('Contact-About with a single child', () => {
  const contactAbout = shallow(
    <ContactAbout>
      {'position'}
      {false}
    </ContactAbout>
  )

  it('contains that child inside of a <span>', () => {
    expect(contactAbout.children().length).toBe(1)
    expect(contactAbout.contains(<span>position</span>)).toBe(true)
  })
})

describe('Contact-About with children', () => {
  const contactAbout = shallow(
    <ContactAbout>
      {'position'}
      {'location'}
    </ContactAbout>
  )

  it('contains these children, each wrapped in <span>', () => {
    expect(contactAbout.children().length).toBe(2)
    expect(contactAbout.contains(<span>position</span>)).toBe(true)
    expect(contactAbout.contains(<span>location</span>)).toBe(true)
  })
})
