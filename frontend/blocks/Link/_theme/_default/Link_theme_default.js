import { withBemMod } from '@bem-react/core'

import { cnLink } from '../../Link'
import './Link_theme_default.css'

export const LinkThemeDefault = withBemMod(cnLink(), { theme: 'default' })
